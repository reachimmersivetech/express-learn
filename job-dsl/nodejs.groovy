job('NodeJS example') {
	scm {
		git('https://reachimmersivetech@bitbucket.org/reachimmersivetech/express-learn.git') { node -> 
			node / gitConfigName('DSL user')
			node / gitConfigEmail('mikemonji@gmail.com')
		}
	}
	triggers {
		scm('H/5 * * * *')
	}
	wrappers {
		nodejs('NodeJS')
	}
	steps {
		shell('npm i')
	}
}

